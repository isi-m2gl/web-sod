package sn.kader.service.on.demand.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.kader.service.on.demand.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {}
