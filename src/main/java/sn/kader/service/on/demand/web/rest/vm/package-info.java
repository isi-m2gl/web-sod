/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.kader.service.on.demand.web.rest.vm;
