package sn.kader.service.on.demand;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.kader.service.on.demand");

        noClasses()
            .that()
            .resideInAnyPackage("sn.kader.service.on.demand.service..")
            .or()
            .resideInAnyPackage("sn.kader.service.on.demand.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..sn.kader.service.on.demand.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
